This package should behave normally with dpkg-source -x for adding and
removing modifications.

For upgrading to a new upstream version, the source tarball can be created
from the git repository at git://salsa.debian.org/games-team/angband.git
by invoking origtargz, or downloaded from http://rephial.org/release/x.y.z 
where x y and z are the major, minor and release version numbers.

Please note that this upstream source tarball must be repacked for two
reasons:

1. The upstream tarball unpacks into a directory called angband-vX.Y.Z,
where Debian wants to use angband-X.Y.Z (no 'v'). (The tarball must also be
renamed to the debian standard format angband_x.y.z.orig.tar.gz)

2. The upstream tarball contains non-free sound, graphics and font files
which must be removed *prior* to running dpkg-source. See 
debian/patches/remove-nonfree.patch and debian/patches/fix-fonts.patch
for accompanying modifications to associated files. Using origtargz 
removes these files for you, but the tarballs downloaded from rephial.org
will contain them.
